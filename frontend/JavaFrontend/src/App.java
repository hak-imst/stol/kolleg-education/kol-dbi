import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class App {
    public static void main(String[] args) throws Exception {

        System.out.println("Hello, World!");

        DbController.connect();
        DbController.createNewTable();
        DbController.insert();

    }
}

class DbController {

    static Connection conn;

    public static void connect() {
        String url = "jdbc:sqlite:./mydb.db";

        try {
            conn = DriverManager.getConnection(url);
            if(conn != null){
                System.out.println("Connection established!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void createNewTable(){
        String sql = "CREATE TABLE IF NOT EXISTS users (id integer PRIMARY KEY, name text NOT NULL);";
        try {
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            System.out.println("Table created!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insert(){
        String sql = "INSERT INTO users (id,name) VALUES (?,?)";

        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, 6);
            pstmt.setString(2, "Antonia");
            pstmt.executeUpdate();
            System.out.println("Seppele inserted");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }

}