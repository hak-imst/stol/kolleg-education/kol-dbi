use w3c;

-- INSERT INTO products (products.ProductID, products.ProductName) 
-- VALUES (78, "Mein Superprodukt");

-- Würde einen FOREING KEY Constraint error auslösen:
-- INSERT INTO products (products.ProductID, products.ProductName, products.CategoryID) 
-- VALUES (79, "Mein Hyperprodukt",7294727);

SELECT products.ProductID, products.ProductName, products.Price, categories.CategoryName
FROM products
JOIN categories ON products.CategoryID = categories.CategoryID
ORDER BY products.ProductID DESC
LIMIT 3;

SELECT products.ProductID, products.ProductName, products.Price, categories.CategoryName
FROM products
RIGHT JOIN categories ON products.CategoryID = categories.CategoryID
ORDER BY products.ProductID DESC
LIMIT 3;
SELECT products.ProductID, products.ProductName, products.Price, categories.CategoryName
FROM products
LEFT JOIN categories ON products.CategoryID = categories.CategoryID
ORDER BY products.ProductID DESC
LIMIT 3;

SELECT products.ProductID, products.ProductName, products.Price, categories.CategoryName
FROM products
LEFT JOIN categories ON products.CategoryID = categories.CategoryID
WHERE categories.CategoryID is null
ORDER BY products.ProductID DESC
LIMIT 3;

SELECT COUNT(*)
FROM products
RIGHT JOIN categories ON products.CategoryID = categories.CategoryID
ORDER BY products.ProductID DESC
LIMIT 3;


SELECT COUNT(*)
FROM products
LEFT JOIN categories ON products.CategoryID = categories.CategoryID
ORDER BY products.ProductID DESC
LIMIT 3;