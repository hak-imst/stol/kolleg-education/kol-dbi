## Command Line Work

Beispiel Befehl um mit Docker direkt SQL Datei auszuführen

```mysql
docker exec -i mysql mysql -t -uroot -p123 kol_dbi < left_join.sql
```

Selbes Beispiel ohne Docker (z.B. XAMPP)

```mysql
mysql -t -uroot -p123 kol_dbi < left_join.sql
```

Mit Docker erst per CLI Datenbank anlegen:

```mysql
echo "CREATE DATABASE beispieldatenbank_w3c" | docker exec -i mysql mysql -t -uroot -p123
```

Dann w3c Datenbank importieren genau wie oben:

```mysql
docker exec -i mysql mysql -t -uroot -p123 beispieldatenbank_w3c < beispieldatenbank_w3c.sql
```