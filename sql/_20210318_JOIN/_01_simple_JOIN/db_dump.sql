SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `Emails`;
CREATE TABLE `Emails` (
  `id` int NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `personen_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `personen_id` (`personen_id`),
  CONSTRAINT `Emails_ibfk_1` FOREIGN KEY (`personen_id`) REFERENCES `Personen` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `Emails` (`id`, `url`, `personen_id`) VALUES
(1,	'st.stolz@gmail.com',	2),
(2,	's.stolz@tsn.at',	2),
(3,	'phmaar@tsn.at',	3);

DROP TABLE IF EXISTS `Personen`;
CREATE TABLE `Personen` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `Personen` (`id`, `name`) VALUES
(1,	'Thomas'),
(2,	'Stefan'),
(3,	'Philipp');