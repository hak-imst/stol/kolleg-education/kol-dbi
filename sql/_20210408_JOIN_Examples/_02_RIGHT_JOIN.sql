-- Zeige alle Produkte mit ihrem Lieferant (supplier).
-- Es sollen auch Produkte ausgegeben werden, denen kein Lieferant zugeordnet worden ist. 

use w3c;

SELECT products.ProductID as Produktnummer, suppliers.SupplierName as Lieferant FROM suppliers 
    RIGHT JOIN products ON products.SupplierID = suppliers.SupplierID
    LIMIT 10;