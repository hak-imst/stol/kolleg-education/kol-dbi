-- Zeige die Bestellung 10248 inklusive ihrer bestellten Positionen. 
-- Die Bestellung soll auch angezeigt werden, wenn keine Positionen zugeordnet wurden.

use w3c;

SELECT orders.OrderID as Bestellnummer, orderdetails.ProductID as Produktnummer FROM orders 
    LEFT JOIN orderdetails ON orders.OrderID = orderdetails.OrderID
    WHERE orders.OrderID = 10248;