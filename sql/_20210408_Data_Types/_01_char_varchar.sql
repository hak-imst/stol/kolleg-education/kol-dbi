use kol_dbi;

CREATE TABLE Teachers (
    id int not null auto_increment,
    LastName varchar(255),
    -- Wenn man für ShortName varchar verwenden würde, würde jeder 5 anstatt 4 Bytes verschwenden
    ShortName char(4),
    primary key(id)
);

INSERT INTO Teachers (LastName, ShortName) VALUES ('Scharmer', 'SALE'), ('Stolz', 'STOL'), ('Landerer', 'LAND');